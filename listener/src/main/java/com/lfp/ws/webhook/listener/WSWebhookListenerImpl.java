package com.lfp.ws.webhook.listener;

import java.util.Date;
import java.util.Objects;
import java.util.Optional;
import java.util.function.Consumer;

import javax.annotation.Nullable;

import org.apache.commons.lang3.Validate;
import org.threadly.concurrent.future.ListenableFuture;
import org.threadly.concurrent.future.SettableListenableFuture;

import com.lfp.joe.core.config.MachineConfig;
import com.lfp.joe.core.function.Scrapable;
import com.lfp.joe.events.eventbus.Registration;
import com.lfp.joe.events.eventbus.ValueNotifier;
import com.lfp.joe.net.http.headers.Headers;
import com.lfp.joe.net.http.uri.URIs;
import com.lfp.joe.retrofit.Services;
import com.lfp.joe.retrofit.client.RFit;
import com.lfp.joe.serial.Serials;
import com.lfp.joe.threads.Threads;
import com.lfp.joe.utils.Utils;
import com.lfp.ws.webhook.service.WSWebhookService;

import at.favre.lib.bytes.Bytes;
import io.mikael.urlbuilder.UrlBuilder;
import okhttp3.Request;
import okhttp3.Response;
import okhttp3.WebSocket;
import okhttp3.WebSocketListener;
import okio.ByteString;

public class WSWebhookListenerImpl extends WebSocketListener implements WSWebhookListener {

	private final Scrapable _scrapable = Scrapable.create();
	private final ValueNotifier.Writeable<Event> valueNotifier = ValueNotifier.createWriteable();
	private final String endpoint;
	private Throwable webSocketError;

	public WSWebhookListenerImpl(WSWebhookService wsWebhookService, String endpoint, Date expiresAt) {
		this(RFit.Calls.consume(wsWebhookService.getToken(URIs.normalizePath(endpoint), false, true, expiresAt)),
				endpoint);
	}

	public WSWebhookListenerImpl(String token, String endpoint) {
		if (!MachineConfig.isDeveloper())
			Validate.isTrue(Utils.Strings.isNotBlank(token), "token is required");
		this.endpoint = URIs.normalizePath(endpoint);
		var serviceConfig = WSWebhookService.SERVICE_CONFIG;
		String path = URIs.normalizePath(WSWebhookService.SOCKET_PATH) + this.endpoint;
		var uri = UrlBuilder.fromUri(serviceConfig.uri()).withPath(path).toUri();
		var client = Services.INSTANCE.client(serviceConfig.proxyURI()).newBuilder().addInterceptor(chain -> {
			var headerStream = Utils.Lots.stream(serviceConfig.headers()).flatMapValues(Utils.Lots::stream);
			headerStream = headerStream.filterKeys(k -> !Utils.Strings.equalsIgnoreCase(k, Headers.AUTHORIZATION));
			headerStream = headerStream.filterKeys(Utils.Strings::isNotBlank);
			headerStream = headerStream.filterValues(Utils.Strings::isNotBlank);
			headerStream = headerStream.append(Headers.AUTHORIZATION, token);
			var rb = chain.request().newBuilder();
			headerStream.forEach(ent -> rb.header(ent.getKey(), ent.getValue()));
			return chain.proceed(rb.build());
		}).build();
		var webSocket = client.newWebSocket(new Request.Builder().url(uri.toString()).build(), this);
		this.onScrap(() -> {
			Utils.Functions.catching(() -> webSocket.close(1000, "scrapping connection"), t -> null);
		});
	}

	@Override
	public Registration listen(Consumer<Event> eventConsumer) {
		Objects.requireNonNull(eventConsumer);
		return this.valueNotifier.getEventBus().register(ebusEvt -> {
			var op = ebusEvt.value();
			if (op.isPresent())
				eventConsumer.accept(op.get());
		});

	}

	@Override
	public ListenableFuture<Event> asFuture() {
		SettableListenableFuture<Event> future = new SettableListenableFuture<>(false);
		this.onScrap(() -> {
			if (webSocketError != null)
				future.setFailure(webSocketError);
			else
				Threads.Futures.cancel(future, true);
		});
		Consumer<Optional<Event>> completeConsumer = op -> {
			if (op.isEmpty())
				return;
			future.setResult(op.get());
		};
		completeConsumer.accept(this.valueNotifier.getValue());
		if (future.isDone())
			return future;
		this.valueNotifier.getEventBus().register(ebusEvent -> {
			completeConsumer.accept(ebusEvent.value());
		}, future);
		completeConsumer.accept(this.valueNotifier.getValue());
		return future;
	}

	@Override
	public void onMessage(WebSocket webSocket, String text) {
		onMessage(Utils.Bits.from(text));
	}

	@Override
	public void onMessage(WebSocket webSocket, ByteString bytes) {
		onMessage(Utils.Bits.from(bytes.toByteArray()));
	}

	private void onMessage(Bytes bytes) {
		this.valueNotifier.set(Serials.Gsons.fromBytes(bytes, Event.class));

	}

	@Override
	public void onClosed(WebSocket webSocket, int code, String reason) {
		this.scrap();
	}

	@Override
	public void onFailure(WebSocket webSocket, Throwable error, @Nullable Response response) {
		this.webSocketError = error;
		this.scrap();
	}

	@Override
	public String getEndpoint() {
		return endpoint;
	}

	@Override
	public boolean isScrapped() {
		return _scrapable.isScrapped();
	}

	@Override
	public Scrapable onScrap(Runnable task) {
		return _scrapable.onScrap(task);
	}

	@Override
	public boolean scrap() {
		return _scrapable.scrap();
	}

}
