package com.lfp.ws.webhook.listener;

import java.util.function.Consumer;

import org.threadly.concurrent.future.ListenableFuture;

import com.lfp.joe.core.function.Scrapable;
import com.lfp.joe.events.eventbus.Registration;

public interface WSWebhookListener extends Scrapable {

	String getEndpoint();

	Registration listen(Consumer<Event> eventConsumer);

	ListenableFuture<Event> asFuture();

}
