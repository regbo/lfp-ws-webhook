package com.lfp.ws.webhook.server.config;

import java.net.URI;
import java.util.List;

import org.aeonbits.owner.Config;

import com.lfp.joe.core.properties.Configs;
import com.lfp.joe.core.properties.code.PrintOptions;
import com.lfp.joe.core.properties.converter.URIConverter;

public interface WSWebhookServerConfig extends Config {

	List<String> jwtIssuerHosts();

	@DefaultValue("HS256")
	String jwtSignatureAlgorithm();
	
	String jwtSigningKey();

	List<String> adminGroupIds();

	@DefaultValue("false")
	boolean developerAdmin();

	@DefaultValue("groupIds")
	String groupIdsClaim();

	@DefaultValue("endpoint")
	String endpointClaim();

	@DefaultValue("hook")
	String hookClaim();

	@DefaultValue("socket")
	String socketClaim();

	@DefaultValue("sockethook")
	String sockethookExecutable();

	@ConverterClass(URIConverter.class) // default 1234 does not work on windows
	@DefaultValue("http://localhost:12345")
	URI sockethookURI();

	public static void main(String[] args) {
		Configs.printProperties(PrintOptions.properties());
		Configs.printProperties(PrintOptions.jsonBuilder().withIncludeValues(true).build());
	}
}
