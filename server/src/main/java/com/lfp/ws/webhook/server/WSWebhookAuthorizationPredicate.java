package com.lfp.ws.webhook.server;

import java.net.URI;
import java.util.Collection;
import java.util.List;
import java.util.regex.Pattern;

import com.lfp.connect.undertow.UndertowUtils;
import com.lfp.connect.undertow.handler.AuthorizationPredicate;
import com.lfp.joe.core.config.MachineConfig;
import com.lfp.joe.core.properties.Configs;
import com.lfp.joe.jwt.Jwts;
import com.lfp.joe.jwt.resolver.IssuerJwkSigningKeyResolver;
import com.lfp.joe.jwt.resolver.SigningKeyRequestFilters;
import com.lfp.joe.jwt.token.JwtLFP;
import com.lfp.joe.net.http.uri.URIs;
import com.lfp.joe.utils.Utils;
import com.lfp.ws.webhook.server.config.WSWebhookServerConfig;
import com.lfp.ws.webhook.service.WSWebhookService;

import at.favre.lib.bytes.Bytes;
import io.jsonwebtoken.Header;
import io.undertow.server.HttpServerExchange;

public class WSWebhookAuthorizationPredicate implements AuthorizationPredicate {

	private final IssuerJwkSigningKeyResolver signingKeyResolver = IssuerJwkSigningKeyResolver
			.of(SigningKeyRequestFilters.issuerHostsEquals(Configs.get(WSWebhookServerConfig.class).jwtIssuerHosts()));

	@SuppressWarnings("rawtypes")
	@Override
	public boolean isAuthorized(HttpServerExchange httpServerExchange, Candidate candidate) {
		var cfg = Configs.get(WSWebhookServerConfig.class);
		URI requestURI = UndertowUtils.getRequestURI(httpServerExchange);
		if (isMember(candidate, cfg.adminGroupIds()))
			return true;
		var jwtSigningKey = cfg.jwtSigningKey();
		JwtLFP<? extends Header> jwt;
		if (Utils.Strings.isBlank(jwtSigningKey))
			jwt = null;
		else
			jwt = Jwts.tryParse(candidate.value(), Bytes.from(jwtSigningKey)).orElse(null);
		if (jwt != null) {
			String path = URIs.normalizePath(requestURI.getPath());
			boolean serviceOk;
			if (URIs.pathPrefixMatch(path, WSWebhookService.HOOK_PATH)
					&& Boolean.TRUE.equals(jwt.getBody().get(cfg.hookClaim()))) {
				serviceOk = true;
			} else if (URIs.pathPrefixMatch(path, WSWebhookService.SOCKET_PATH)
					&& Boolean.TRUE.equals(jwt.getBody().get(cfg.socketClaim()))) {
				serviceOk = true;
			} else
				serviceOk = false;
			if (serviceOk && hasEndpointClaim(jwt, path)) {
				return true;
			}
		}
		if (MachineConfig.isDeveloper() && cfg.developerAdmin())
			return true;
		return false;
	}

	private boolean hasEndpointClaim(JwtLFP<?> jwt, String path) {
		path = URIs.normalizePath(path);
		String endpoint = "/" + Utils.Lots.stream(path.split(Pattern.quote("/"))).skip(2).joining("/");
		var claims = jwt.getBody();
		var cfg = Configs.get(WSWebhookServerConfig.class);
		var verifiedEndpointPrefix = Utils.Types.tryCast(claims.get(cfg.endpointClaim()), String.class).orElse(null);
		if (Utils.Strings.isBlank(verifiedEndpointPrefix))
			return false;
		return URIs.pathPrefixMatch(endpoint, verifiedEndpointPrefix);
	}

	private boolean isMember(Candidate candidate, List<String> groupIds) {
		if (groupIds == null || groupIds.isEmpty())
			return false;
		var jws = Jwts.tryParse(candidate.value(), this.signingKeyResolver).orElse(null);
		if (jws == null)
			return false;
		var cfg = Configs.get(WSWebhookServerConfig.class);
		var obj = jws.getBody().get(cfg.groupIdsClaim());
		if (!(obj instanceof Collection))
			return false;
		for (var gidObj : (Collection<?>) obj) {
			if (groupIds.contains(gidObj))
				return true;
		}
		return false;
	}

	public static void main(String[] args) {
		String jwt = "";
		var pred = new WSWebhookAuthorizationPredicate();
		System.out.println(Jwts.tryParse(jwt, pred.signingKeyResolver).get());
	}
}
