package com.lfp.ws.webhook.server;

import java.time.Duration;
import java.util.Date;

import org.apache.commons.lang3.Validate;

import com.lfp.joe.core.properties.Configs;
import com.lfp.joe.jwt.Jwts;
import com.lfp.joe.jwt.builder.JwtBuilderLFP;
import com.lfp.joe.net.http.uri.URIs;
import com.lfp.joe.retrofit.CallLFP;
import com.lfp.joe.utils.Utils;
import com.lfp.ws.webhook.server.config.WSWebhookServerConfig;
import com.lfp.ws.webhook.service.WSWebhookService;

import io.jsonwebtoken.SignatureAlgorithm;
import okhttp3.RequestBody;
import retrofit2.Call;

public class WSWebhookServiceImpl implements WSWebhookService {

	@Override
	public Call<Void> hookGet(String endpoint) {
		return CallLFP.error(new UnsupportedOperationException("hookGet should be proxied to go server"));
	}

	@Override
	public Call<Void> hookPost(String endpoint, RequestBody requestBody) {
		return CallLFP.error(new UnsupportedOperationException("hookPost should be proxied to go server"));
	}

	@Override
	public Call<String> getToken(String endpoint, boolean hookEnabled, boolean socketEnabled, Date expiresAt) {
		return CallLFP.success(() -> getTokenInternal(endpoint, hookEnabled, socketEnabled, expiresAt));
	}

	protected String getTokenInternal(String endpoint, boolean hookEnabled, boolean socketEnabled, Date expiresAt) {
		endpoint = URIs.normalizePath(endpoint);
		Validate.isTrue(hookEnabled || socketEnabled, "hook or socket access must be requested");
		var cfg = Configs.get(WSWebhookServerConfig.class);
		var alg = SignatureAlgorithm.forName(cfg.jwtSignatureAlgorithm());
		var key = Utils.Bits.parseBase64Url(cfg.jwtSigningKey());
		var jwtBuilder = new JwtBuilderLFP().signWith(alg, key.array());
		if (expiresAt != null)
			jwtBuilder = jwtBuilder.setExpiration(expiresAt);
		jwtBuilder = jwtBuilder.claim(cfg.endpointClaim(), URIs.normalizePath(endpoint));
		if (hookEnabled)
			jwtBuilder = jwtBuilder.claim(cfg.hookClaim(), true);
		if (socketEnabled)
			jwtBuilder = jwtBuilder.claim(cfg.socketClaim(), true);
		return jwtBuilder.compact();
	}

	public static void main(String[] args) {
		var key = Configs.get(WSWebhookServerConfig.class).jwtSigningKey();
		System.out.println(key);
		var impl = new WSWebhookServiceImpl();
		var token = impl.getTokenInternal("hat", true, true,
				new Date(System.currentTimeMillis() + Duration.ofDays(1).toMillis()));
		System.out.println(token);
		var parsed = Jwts.tryParse(token, Utils.Bits.parseBase64(key)).get();
		System.out.println(parsed);
	}
}
