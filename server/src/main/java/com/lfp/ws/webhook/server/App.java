package com.lfp.ws.webhook.server;

import java.io.File;
import java.io.IOException;
import java.util.Arrays;
import java.util.LinkedHashSet;
import java.util.List;

import com.lfp.connect.undertow.UndertowUtils;
import com.lfp.connect.undertow.Undertows;
import com.lfp.connect.undertow.config.UndertowConfig;
import com.lfp.connect.undertow.handler.ErrorLoggingHandler;
import com.lfp.connect.undertow.handler.LoadBalancingProxyClientLFP;
import com.lfp.connect.undertow.handler.MessageHandler;
import com.lfp.connect.undertow.handler.ThreadHttpHandler;
import com.lfp.connect.undertow.retrofit.RetrofitHandler;
import com.lfp.joe.core.config.MachineConfig;
import com.lfp.joe.core.properties.Configs;
import com.lfp.joe.net.http.uri.URIs;
import com.lfp.joe.process.Procs;
import com.lfp.joe.threads.Threads;
import com.lfp.joe.utils.Utils;
import com.lfp.ws.webhook.server.config.WSWebhookServerConfig;
import com.lfp.ws.webhook.service.WSWebhookService;

import io.undertow.Handlers;
import io.undertow.server.HttpHandler;
import io.undertow.util.Headers;
import io.undertow.util.StatusCodes;

public class App {
	private static final Class<?> THIS_CLASS = new Object() {}.getClass().getEnclosingClass();
	private static final org.slf4j.Logger logger = org.slf4j.LoggerFactory.getLogger(THIS_CLASS);

	public static void main(String[] args) throws InterruptedException {
		startSockethook();
		var serverBuilder = Undertows.serverBuilderBase();
		serverBuilder.addHttpListener(Configs.get(UndertowConfig.class).defaultPort(), "0.0.0.0", createHttpHandler());
		var server = serverBuilder.build();
		server.start();
		Utils.Lots.stream(server.getListenerInfo()).forEach(v -> {
			logger.info("listening on:{}", v);
		});
		Thread.currentThread().join();
	}

	private static HttpHandler createHttpHandler() {
		var lbClient = new LoadBalancingProxyClientLFP();
		var uri = Configs.get(WSWebhookServerConfig.class).sockethookURI();
		lbClient.addHost(uri);
		HttpHandler handler = Handlers.proxyHandler(lbClient);
		handler = normalizeApplicationJson(handler);
		var serviceHandler = new RetrofitHandler<>(WSWebhookService.class, new WSWebhookServiceImpl());
		handler = Handlers.predicate(hsx -> {
			var requestURI = UndertowUtils.getRequestURI(hsx);
			var isAdmin = URIs.pathPrefixMatch(requestURI.getPath(), WSWebhookService.ADMIN_PATH);
			return isAdmin;
		}, serviceHandler, handler);
		handler = new ThreadHttpHandler(handler);
		handler = new ErrorLoggingHandler(handler);
		handler = Handlers.predicate(new WSWebhookAuthorizationPredicate(), handler,
				new MessageHandler(StatusCodes.UNAUTHORIZED));
		return handler;
	}

	private static HttpHandler normalizeApplicationJson(HttpHandler handler) {
		String contentTypeValue = "application/json";
		return hsx -> {
			var headerStream = UndertowUtils.streamHeaders(hsx.getRequestHeaders());
			headerStream = headerStream.filterKeys(Headers.CONTENT_TYPE.toString()::equalsIgnoreCase);
			headerStream = headerStream.filterValues(v -> {
				if (!Utils.Strings.startsWithIgnoreCase(v, contentTypeValue))
					return false;
				if (Utils.Strings.equals(v, contentTypeValue))
					return false;
				return true;
			});
			var removeHeaders = headerStream.grouping();
			for (var name : removeHeaders.keySet()) {
				var removeValues = removeHeaders.get(name);
				var currentValues = hsx.getRequestHeaders().remove(name);
				var newValues = new LinkedHashSet<String>();
				newValues.add(contentTypeValue);
				for (var value : currentValues)
					if (!removeValues.contains(value))
						newValues.add(value);
				hsx.getRequestHeaders().addAll(Headers.CONTENT_TYPE, newValues);
			}
			handler.handleRequest(hsx);
		};
	}

	private static void startSockethook() {
		var cfg = Configs.get(WSWebhookServerConfig.class);
		var sockethookExecutable = cfg.sockethookExecutable();
		if (MachineConfig.isDeveloper())
			try {
				Procs.start("taskkill", "/f", "/im", sockethookExecutable + ".exe").join();
			} catch (IOException e) {
				throw RuntimeException.class.isInstance(e) ? RuntimeException.class.cast(e) : new RuntimeException(e);
			}
		var programLogger = org.slf4j.LoggerFactory.getLogger(THIS_CLASS.getName() + " - " + sockethookExecutable);
		try {
			List<String> args = Arrays.asList("--port", "" + cfg.sockethookURI().getPort());
			List<String> fullCmds;
			if (!Utils.Machine.isWindows()) {
				String linuxExecutable = "/root/go/bin/" + sockethookExecutable;
				if (!new File(linuxExecutable).exists())
					linuxExecutable = sockethookExecutable;
				fullCmds = Utils.Lots.stream(args).prepend(linuxExecutable).toList();
			} else
				fullCmds = Utils.Lots.stream(args).prepend(sockethookExecutable).toList();
			var pe = Procs.start(fullCmds, pem -> {
				pem.logOutput(logger);
			});
			Threads.Futures.callback(pe.getFuture(), (v, t) -> {
				String msg = String.format("%s exited. code:%s", sockethookExecutable,
						v == null ? null : v.getExitValue());
				programLogger.error(msg, t);
			});
		} catch (Throwable t) {
			logger.error("could not start program:{}", sockethookExecutable, t);
		}
	}

}
