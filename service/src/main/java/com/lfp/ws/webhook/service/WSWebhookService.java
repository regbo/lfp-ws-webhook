package com.lfp.ws.webhook.service;

import java.util.Date;

import com.google.gson.JsonElement;
import com.lfp.joe.core.properties.Configs;
import com.lfp.joe.utils.Utils;
import com.lfp.ws.webhook.service.config.WSWebhookServiceConfig;

import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.Headers;
import retrofit2.http.POST;
import retrofit2.http.Path;
import retrofit2.http.Query;

public interface WSWebhookService {

	public static WSWebhookServiceConfig SERVICE_CONFIG = Configs.get(WSWebhookServiceConfig.class);
	public static String SOCKET_PATH = "/socket";
	public static String HOOK_PATH = "/hook";
	public static String ADMIN_PATH = "/admin";

	default String socket(String groupId, String endpoint) {
		if (Utils.Strings.isBlank(groupId))
			groupId = "_";
		return String.format("/%s/%s/%s", SOCKET_PATH, groupId, endpoint);
	}

	@GET(HOOK_PATH + "{endpoint}")
	Call<Void> hookGet(@Path(value = "endpoint", encoded = true) String endpoint);

	@POST(HOOK_PATH + "{endpoint}")
	Call<Void> hookPost(@Path(value = "endpoint", encoded = true) String endpoint, @Body RequestBody requestBody);

	@GET(ADMIN_PATH)
	Call<String> getToken(@Query("endpoint") String endpoint, @Query("hook") boolean hook,
			@Query("socket") boolean socket, @Query("expiresAt") Date expiresAt);

}
