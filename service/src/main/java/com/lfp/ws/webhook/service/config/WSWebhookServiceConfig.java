package com.lfp.ws.webhook.service.config;

import com.lfp.joe.core.properties.Configs;
import com.lfp.joe.core.properties.code.PrintOptions;
import com.lfp.joe.net.http.oauth.OauthServiceConfig;

public interface WSWebhookServiceConfig extends OauthServiceConfig {

	public static void main(String[] args) {
		Configs.printProperties(PrintOptions.propertiesBuilder().build());
		Configs.printProperties(PrintOptions.jsonBuilder().withSkipPopulated(true).build());
	}
}
