package test;

import java.io.IOException;
import java.security.KeyManagementException;
import java.security.NoSuchAlgorithmException;
import java.security.cert.CertificateException;
import java.security.cert.X509Certificate;
import java.time.Duration;
import java.util.Date;
import java.util.concurrent.TimeUnit;

import javax.net.ssl.HostnameVerifier;
import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLSession;
import javax.net.ssl.SSLSocketFactory;
import javax.net.ssl.TrustManager;
import javax.net.ssl.X509TrustManager;

import com.google.common.base.Stopwatch;
import com.google.common.collect.ImmutableMap;
import com.lfp.joe.net.proxy.Proxy;
import com.lfp.joe.okhttp.Ok;
import com.lfp.joe.retrofit.client.RFit;
import com.lfp.joe.serial.Serials;
import com.lfp.ws.webhook.service.WSWebhookService;

import okhttp3.MediaType;
import okhttp3.OkHttpClient;
import okhttp3.RequestBody;

public class ServiceConfigGetTest {

	public static void main(String[] args) throws IOException, KeyManagementException, NoSuchAlgorithmException {
		WSWebhookService service = null;
		for (int i = 0; i < 1; i++) {
			var sw = Stopwatch.createStarted();
			service = RFit.Clients.get(WSWebhookService.class);
			System.out.println(sw.elapsed(TimeUnit.MILLISECONDS));
		}
		service = RFit.Clients.builder(getClient(), WSWebhookService.SERVICE_CONFIG, null, null).build()
				.create(WSWebhookService.class);
		if (true) {
			var call = service.getToken("/sms/2155154824", true, true,
					new Date(System.currentTimeMillis() + Duration.ofDays(1).toMillis()));
			var body = RFit.Calls.consume(call);
			System.out.println(body);
		}
		if (false) {
			var data = ImmutableMap.of("msg", "the time is:" + new Date());
			var call = service.hookPost("/sms/2155154824",
					RequestBody.create(MediaType.parse("application/json"), Serials.Gsons.toBytes(data).array()));
			var body = RFit.Calls.consume(call);
			System.out.println(body);
		}
	}

	private static OkHttpClient getClient() throws KeyManagementException, NoSuchAlgorithmException {
		var clientBuilder = Ok.Clients
				.get(Proxy.builder().type(Proxy.Type.HTTP).hostname("localhost").port(8888).build()).newBuilder();
		final TrustManager[] trustAllCerts = new TrustManager[] { new X509TrustManager() {
			@Override
			public void checkClientTrusted(java.security.cert.X509Certificate[] chain, String authType)
					throws CertificateException {
			}

			@Override
			public void checkServerTrusted(java.security.cert.X509Certificate[] chain, String authType)
					throws CertificateException {
			}

			@Override
			public java.security.cert.X509Certificate[] getAcceptedIssuers() {
				return new X509Certificate[0];
			}
		} };

		// Install the all-trusting trust manager
		final SSLContext sslContext = SSLContext.getInstance("SSL");
		sslContext.init(null, trustAllCerts, new java.security.SecureRandom());
		// Create an ssl socket factory with our all-trusting manager
		final SSLSocketFactory sslSocketFactory = sslContext.getSocketFactory();
		return clientBuilder.sslSocketFactory(sslSocketFactory, (X509TrustManager) trustAllCerts[0])
				.hostnameVerifier(new HostnameVerifier() {
					@Override
					public boolean verify(String hostname, SSLSession session) {
						return true;
					}
				}).build();
	}
}
