package test;

import com.lfp.joe.okhttp.Ok;

import okhttp3.Request;
import okhttp3.WebSocket;
import okhttp3.WebSocketListener;

public class ListenTest {

	public static void main(String[] args) throws InterruptedException {
		String url = "wss://ws-webhook-api.iplasso.com/socket/sms/2155154824?access_token=eyJhbGciOiJIUzI1NiJ9.eyJpYXQiOjE1OTg0NTg5ODcsImVuZHBvaW50IjoiL3Ntcy8yMTU1MTU0ODI0Iiwic29ja2V0Ijp0cnVlfQ.sP45XFRRPSneYgececKeEGPL_i27XWQ_6794Av39PM4";
		Ok.Clients.get().newWebSocket(new Request.Builder().url(url).build(), new WebSocketListener() {

			/** Invoked when a text (type {@code 0x1}) message has been received. */
			public void onMessage(WebSocket webSocket, String text) {
				System.out.println(text);
			}
		});
		Thread.currentThread().join();
	}
}
